﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Satelital.Emissor.Model;
using Satelital.Emissor.Repository;

namespace Satelital.Emissor.Service
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService (IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public User Authenticate(string username, string password)
        {
            if (String.IsNullOrWhiteSpace(username)) throw new InvalidOperationException("Você precisa informar o nome de usuário");
            if (String.IsNullOrWhiteSpace(password)) throw new InvalidOperationException("Você precisa informar a senha");

            var user = GetUser(username, password);
            if (user == null) throw new InvalidOperationException("Usuário ou senha inválidos");

            return user;
        }

        public User AddUser(string name, string username, string password, string confirmPassword, UserRole userRole)
        {
            if (HasUser(username)) throw new InvalidOperationException("Usuário já cadastrado");
            if (String.IsNullOrWhiteSpace(username)) throw new InvalidOperationException("Você precisa informar o nome de usuário");
            if (String.IsNullOrWhiteSpace(password)) throw new InvalidOperationException("Você precisa informar a senha");
            if (password != confirmPassword) throw new InvalidOperationException("Confirme a senha corretamente");

            var user = new User(name, username, password, userRole);
            _userRepository.Add(user);
            _userRepository.SaveChanges();

            return user;
        }

        public User UpdateUser(int userId, string name, string username, string password, string confirmPassword, UserRole userRole)
        {
            var user = GetUserById(userId);
            
            if (HasUser(username) && user.Username != username) throw new InvalidOperationException("Usuário já cadastrado");
            if (String.IsNullOrWhiteSpace(password)) throw new InvalidOperationException("Você precisa informar a senha");
            if (password != confirmPassword) throw new InvalidOperationException("Confirme a senha corretamente");

            user.Name = name;
            user.Username = username;
            user.Password = password;
            user.UserRole = userRole;

            _userRepository.Edit(user);
            _userRepository.SaveChanges();

            return user;
        }

        public User GetUser(int userId)
        {
            return _userRepository.Find(u => u.Id.Equals(userId)).FirstOrDefault();
        }

        public void DeleteUser(int userId)
        {
            _userRepository.Delete(u=>u.Id.Equals(userId));
            _userRepository.SaveChanges();

        }
        public IList<User> GetUsers()
        {
            return _userRepository.GetAll().ToList();
        }

        private User GetUser(string username, string password)
        {
            return _userRepository
                .Find(u => u.Username.Equals(username) && u.Password.Equals(password))
                .FirstOrDefault();
        }

        private User GetUserById(int userId)
        {
            return _userRepository
                .Find(u => u.Id.Equals(userId))
                .FirstOrDefault();
        }

        private bool HasUser(string username)
        {
            return _userRepository
                .Count(u => u.Username.Equals(username)) > 0;
        }

    }
}
