using System.Collections.Generic;
using Satelital.Emissor.Model;

namespace Satelital.Emissor.Service
{
    public interface IUserService
    {
        User Authenticate(string username, string password);
        IList<User> GetUsers();
        User AddUser(string name, string username, string password, string confirmPassword, UserRole userRole);
        User UpdateUser(int userId, string name, string username, string password, string confirmPassword, UserRole userRole);
        User GetUser(int userId);
        void DeleteUser(int userId);
    }
}