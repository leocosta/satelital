﻿using System.Collections.Generic;
using Satelital.Emissor.Model;

namespace Satelital.Emissor.Service
{
    public interface IPickingListService
    {
        PickingList ImportPickingList(User user, string xlsPathFile);
        void CheckPickingListItem(User checkedBy, PickingList pickingList, string supplierOrderNumber, string altSky);
        void ManualCheckPinkingListItem(User authorizedBy, PickingList pickingList, string supplierOrderNumber, string altsku);
        IList<PickingList> GetPickingLists();
        PickingList GetPickingList(int pickingListId);
    }
}
