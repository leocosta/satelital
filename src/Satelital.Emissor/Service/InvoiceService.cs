﻿using System.Linq;
using Satelital.Emissor.Helper;
using Satelital.Emissor.Infrastructure.Printer.PDF;
using Satelital.Emissor.Infrastructure.Printer.Zebra;
using Satelital.Emissor.Infrastructure.Storage;
using Satelital.Emissor.Model;
using Satelital.Emissor.Repository;

namespace Satelital.Emissor.Service
{
    public class InvoiceService : IInvoiceService
    {
        private readonly IFileManager _fileManager;
        private readonly IInvoiceXmlRepository _invoiceRepository;
        private readonly IZebraPrinter _zebraPrinter;
        private readonly IPDFPrinter _pdfPrinter;

        public InvoiceService(IFileManager fileManager, 
            IInvoiceXmlRepository invoiceRepository, 
            IZebraPrinter zebraPrinter,
            IPDFPrinter pdfPrinter)
        {
            _fileManager = fileManager;
            _invoiceRepository = invoiceRepository;
            _zebraPrinter = zebraPrinter;
            _pdfPrinter = pdfPrinter;
        }

        public Invoice Find(string orderId)
        {
            return _invoiceRepository.FindInvoiceByOrderId(orderId); 
        }

        public void PrintPDF(string referenceFile)
        {
            _pdfPrinter.Print(referenceFile);
        }
        
        public void PrintLabel(Invoice invoice, string templatePath)
        {
            _zebraPrinter.Print(ParseTemplate(invoice, templatePath));
        }

        private static string ParseTemplate(Invoice invoice, string templatePath)
        {
            return invoice
                .GetProperties()
                .ToList()
                .Aggregate(templatePath.GetContentFile(), (t, o)
                    => t.Replace("{" + o.Name.ToString() + "}", o.Value.ToString()));
        }
        
        public void MoveProcessedFiles(string referenceFile, string destinationPath)
        {
            foreach (var file in _fileManager.Find(f => f.Contains(referenceFile)))
                _fileManager.Move(file, file.CombineWithPath(destinationPath));
        }
    }
}
