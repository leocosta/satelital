﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Practices.ObjectBuilder2;
using Satelital.Emissor.Infrastructure.Data.Xls;
using Satelital.Emissor.Model;
using Satelital.Emissor.Repository;

namespace Satelital.Emissor.Service
{
    public class PickingListService : IPickingListService
    {
        private readonly IPickingListRepository _pickingListRepository;
        private readonly IXlsRepository _xlsRepository;

        public PickingListService(IPickingListRepository pickingListRepository, IXlsRepository xlsRepository)
        {
            _pickingListRepository = pickingListRepository;
            _xlsRepository = xlsRepository;
        }

        public PickingList ImportPickingList(User user, string xlsPathFile)
        {

            _xlsRepository.PathFile = xlsPathFile;

            var filename = _xlsRepository.Filename;
            if (HasPickingListImported(filename))
            {
                throw new InvalidOperationException(String.Format("O arquivo {0} já foi importado", filename));
            }
            
            var pickingList = new PickingList(user, filename);

            _xlsRepository.GetAllFrom("Sheet1")
                .Where(item => !String.IsNullOrWhiteSpace(item.DocFornecedor.ToString()))
                .ForEach(f=> 
                        {
                            for (var i = 1; i <= int.Parse(String.IsNullOrWhiteSpace(f.Qtde.ToString()) ? "1" : f.Qtde.ToString()); i++)
                                pickingList.AddPickingListItem(f.DocFornecedor, f.Descricao, 1, f.Sku, f.Altsku);
                        });
            
            _pickingListRepository.Add(pickingList);
            _pickingListRepository.SaveChanges();

            return pickingList;
        }

        public void CheckPickingListItem(User checkedBy, PickingList pickingList, string supplierOrderNumber, string altsku)
        {

            var pickingListItem = pickingList.PickingListItems.Where(
                    p => p.SupplierOrderNumber.Equals(supplierOrderNumber) && p.AltSku.Equals(altsku))
                    .OrderBy(o=>o.IsChecked)
                    .First();

            if (pickingListItem.PickingList.IsAllConferred()) throw new Exception("Todos os itens da lista já foram conferidos");
            if (pickingListItem.IsChecked) throw new Exception("Item já foi conferido");

            pickingListItem.SetChecked(checkedBy);

            _pickingListRepository.Edit(pickingList);
            _pickingListRepository.SaveChanges();
        }


        public void ManualCheckPinkingListItem(User authorizedBy, PickingList pickingList, string supplierOrderNumber, string altsku)
        {
            if (!authorizedBy.IsAdministrator())
                throw new InvalidOperationException("Você precisa ter privilégios de supervisor para checar item manualmente");

            CheckPickingListItem(authorizedBy, pickingList, supplierOrderNumber, altsku);
        }

        public IList<PickingList> GetPickingLists()
        {
            return _pickingListRepository
                .GetAll()
                .OrderByDescending(o=>o.Id)
                .ToList();
        }

        public PickingList GetPickingList(int pickingListId)
        {
            return _pickingListRepository.Find(p => p.Id.Equals(pickingListId)).FirstOrDefault();
        }

        private bool HasPickingListImported(string filename)
        {
            return _pickingListRepository.Count(p => p.Filename.Equals(filename)) > 0;
        }

    }
}
