using Satelital.Emissor.Model;

namespace Satelital.Emissor.Service
{
    public interface IInvoiceService
    {
        Invoice Find(string orderId);
        void PrintPDF(string referenceFile);
        void PrintLabel(Invoice invoice, string templatePath);
        void MoveProcessedFiles(string referenceFile, string destinationPath);
    }
}