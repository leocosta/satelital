﻿using System;
using System.Windows.Forms;
using Satelital.Emissor.IoC;
using Satelital.Emissor.Model;
using Satelital.Emissor.Service;

namespace Satelital.Emissor.Forms
{
    public partial class UserUpdate : Form
    {
        private readonly IUserService _userService;
        private readonly int _userId;

        public UserUpdate(int userId)
        {
            _userService = ServiceLocator.GetInstance<IUserService>();
            _userId = userId;

            InitializeComponent();
            BindUserRole();
            LoadUserData();
        }

        private void LoadUserData()
        {
            var user = _userService.GetUser(_userId);
            FullName.Text = user.Name;
            Username.Text = user.Username;
            Password.Text = user.Password;
            ConfirmPassword.Text = user.Password;
            UserRole.SelectedText = user.UserRole.ToString();
        }

        private void BindUserRole()
        {
            UserRole.DataSource = Enum.GetNames(typeof(UserRole));
        }

        private void Close_Click(object sender, EventArgs e)
        {
            Close();
        }
        
        private void Save_Click(object sender, EventArgs e)
        {
            try
            {
                _userService.UpdateUser(_userId, FullName.Text, Username.Text, Password.Text, ConfirmPassword.Text,
                                     (UserRole)Enum.Parse(typeof(UserRole), UserRole.SelectedValue.ToString(), true));

                MessageBox.Show(@"Usuário atualizado com sucesso!", @"Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, @"Erro", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void Delete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show(@"Deseja realmente excluir usuário?", @"Remover", MessageBoxButtons.YesNo,
                                MessageBoxIcon.Question) != DialogResult.Yes) return;

            _userService.DeleteUser(_userId);
            MessageBox.Show(@"Usuário removido com sucesso!", @"Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            Close();
        }


    }
}
