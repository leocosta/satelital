﻿namespace Satelital.Emissor.Forms
{
    partial class PickingListView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PickingListGrid = new System.Windows.Forms.DataGridView();
            this.Close = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.PickingListGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // PickingListGrid
            // 
            this.PickingListGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.PickingListGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PickingListGrid.Location = new System.Drawing.Point(12, 12);
            this.PickingListGrid.MultiSelect = false;
            this.PickingListGrid.Name = "PickingListGrid";
            this.PickingListGrid.ReadOnly = true;
            this.PickingListGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.PickingListGrid.Size = new System.Drawing.Size(884, 278);
            this.PickingListGrid.TabIndex = 2;
            this.PickingListGrid.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.PickingListGrid_CellClick);
            // 
            // Close
            // 
            this.Close.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Close.Location = new System.Drawing.Point(792, 309);
            this.Close.Name = "Close";
            this.Close.Size = new System.Drawing.Size(104, 29);
            this.Close.TabIndex = 6;
            this.Close.Text = "&Fechar";
            this.Close.UseVisualStyleBackColor = true;
            this.Close.Click += new System.EventHandler(this.Close_Click);
            // 
            // PickingListView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(909, 349);
            this.Controls.Add(this.Close);
            this.Controls.Add(this.PickingListGrid);
            this.Name = "PickingListView";
            this.Text = "Exibir listas";
            ((System.ComponentModel.ISupportInitialize)(this.PickingListGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView PickingListGrid;
        private System.Windows.Forms.Button Close;
    }
}