﻿namespace Satelital.Emissor.FormsUsersGrid
{
    partial class UserList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.UsersGrid = new System.Windows.Forms.DataGridView();
            this.Close = new System.Windows.Forms.Button();
            this.NewUser = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.UsersGrid)).BeginInit();
            this.SuspendLayout();
            // 
            // UsersGrid
            // 
            this.UsersGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.UsersGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.UsersGrid.Location = new System.Drawing.Point(12, 12);
            this.UsersGrid.MultiSelect = false;
            this.UsersGrid.Name = "UsersGrid";
            this.UsersGrid.ReadOnly = true;
            this.UsersGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.UsersGrid.Size = new System.Drawing.Size(603, 232);
            this.UsersGrid.TabIndex = 3;
            this.UsersGrid.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.UsersGrid_CellClick);
            // 
            // Close
            // 
            this.Close.Location = new System.Drawing.Point(510, 249);
            this.Close.Name = "Close";
            this.Close.Size = new System.Drawing.Size(105, 28);
            this.Close.TabIndex = 4;
            this.Close.Text = "&Fechar";
            this.Close.UseVisualStyleBackColor = true;
            this.Close.Click += new System.EventHandler(this.Close_Click);
            // 
            // NewUser
            // 
            this.NewUser.Location = new System.Drawing.Point(12, 250);
            this.NewUser.Name = "NewUser";
            this.NewUser.Size = new System.Drawing.Size(133, 28);
            this.NewUser.TabIndex = 5;
            this.NewUser.Text = "&Novo Usuário";
            this.NewUser.UseVisualStyleBackColor = true;
            this.NewUser.Click += new System.EventHandler(this.button1_Click);
            // 
            // UserList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Close;
            this.ClientSize = new System.Drawing.Size(627, 284);
            this.Controls.Add(this.NewUser);
            this.Controls.Add(this.Close);
            this.Controls.Add(this.UsersGrid);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "UserList";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Usuários";
            ((System.ComponentModel.ISupportInitialize)(this.UsersGrid)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView UsersGrid;
        private System.Windows.Forms.Button Close;
        private System.Windows.Forms.Button NewUser;
    }
}