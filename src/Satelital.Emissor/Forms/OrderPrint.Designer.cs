﻿namespace Satelital.Emissor.Forms
{
    partial class OrderPrint
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ExitApplication = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.OrderId = new System.Windows.Forms.TextBox();
            this.Print = new System.Windows.Forms.Button();
            this.toolStripStatus = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatus.SuspendLayout();
            this.SuspendLayout();
            // 
            // ExitApplication
            // 
            this.ExitApplication.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.ExitApplication.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ExitApplication.Location = new System.Drawing.Point(436, 53);
            this.ExitApplication.Name = "ExitApplication";
            this.ExitApplication.Size = new System.Drawing.Size(93, 26);
            this.ExitApplication.TabIndex = 22;
            this.ExitApplication.Text = "Fechar";
            this.ExitApplication.UseVisualStyleBackColor = true;
            this.ExitApplication.Click += new System.EventHandler(this.ExitApplication_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(32, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(131, 15);
            this.label1.TabIndex = 21;
            this.label1.Text = "Número do Pedido:";
            // 
            // OrderId
            // 
            this.OrderId.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OrderId.Location = new System.Drawing.Point(35, 55);
            this.OrderId.Name = "OrderId";
            this.OrderId.Size = new System.Drawing.Size(295, 24);
            this.OrderId.TabIndex = 19;
            this.OrderId.TextChanged += new System.EventHandler(this.OrderId_TextChanged);
            // 
            // Print
            // 
            this.Print.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Print.Location = new System.Drawing.Point(336, 53);
            this.Print.Name = "Print";
            this.Print.Size = new System.Drawing.Size(94, 26);
            this.Print.TabIndex = 20;
            this.Print.Text = "Imprimir";
            this.Print.UseVisualStyleBackColor = true;
            this.Print.Click += new System.EventHandler(this.Print_Click);
            // 
            // toolStripStatus
            // 
            this.toolStripStatus.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel});
            this.toolStripStatus.Location = new System.Drawing.Point(0, 116);
            this.toolStripStatus.Name = "toolStripStatus";
            this.toolStripStatus.Size = new System.Drawing.Size(540, 22);
            this.toolStripStatus.TabIndex = 23;
            this.toolStripStatus.Text = "statusStrip1";
            // 
            // toolStripStatusLabel
            // 
            this.toolStripStatusLabel.Name = "toolStripStatusLabel";
            this.toolStripStatusLabel.Size = new System.Drawing.Size(0, 17);
            // 
            // OrderPrint
            // 
            this.AcceptButton = this.Print;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.ExitApplication;
            this.ClientSize = new System.Drawing.Size(540, 138);
            this.Controls.Add(this.toolStripStatus);
            this.Controls.Add(this.ExitApplication);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.OrderId);
            this.Controls.Add(this.Print);
            this.Name = "OrderPrint";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Imprimir Pedido";
            this.toolStripStatus.ResumeLayout(false);
            this.toolStripStatus.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button ExitApplication;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox OrderId;
        private System.Windows.Forms.Button Print;
        private System.Windows.Forms.StatusStrip toolStripStatus;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel;
    }
}