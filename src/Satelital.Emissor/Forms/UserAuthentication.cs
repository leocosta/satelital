﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Satelital.Emissor.IoC;
using Satelital.Emissor.Model;
using Satelital.Emissor.Service;

namespace Satelital.Emissor.Forms
{
    public partial class UserAuthentication : Form
    {
        private readonly IUserService _userService;

        public UserAuthentication()
        {
            InitializeComponent();

            _userService = ServiceLocator.GetInstance<IUserService>();
        }

        public User UserAuthenticated { get; protected set; }
        public bool IsUserAuthenticated
        {
            get { return UserAuthenticated != null; }
        }

        private void Authenticate_Click(object sender, EventArgs e)
        {
            try
            {
               UserAuthenticated = _userService.Authenticate(Username.Text, Password.Text);
               Close();
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message, @"Atenção", MessageBoxButtons.OK,MessageBoxIcon.Warning);
                ClarForm();
                if (ex.GetType().Equals(typeof(ProviderIncompatibleException)))
                {
                    new Configuration().ShowDialog();
                    MessageBox.Show(@"Reinicie o sistema e tente novamente", @"Atenção", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Application.Exit();
                }
            }
        }

        private void ClarForm()
        {
            Password.Text = String.Empty;
            Password.Focus();
        }

        private void Close_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
