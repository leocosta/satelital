﻿namespace Satelital.Emissor.Forms
{
    partial class Configuration
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.InvoiceSourcePath = new System.Windows.Forms.TextBox();
            this.InvoiceSourcePathButton = new System.Windows.Forms.Button();
            this.LabelTemplateFileButton = new System.Windows.Forms.Button();
            this.LabelTemplateFile = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.InvoiceDestinationPathButton = new System.Windows.Forms.Button();
            this.InvoiceDestinationPath = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.PdfPrinterName = new System.Windows.Forms.ComboBox();
            this.LabelPrinterName = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.AdobePath = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.AdobePathButton = new System.Windows.Forms.Button();
            this.Close = new System.Windows.Forms.Button();
            this.Save = new System.Windows.Forms.Button();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.openFileDialog = new System.Windows.Forms.OpenFileDialog();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.DbServerName = new System.Windows.Forms.TextBox();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(31, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(88, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Pasta de Origem:";
            // 
            // InvoiceSourcePath
            // 
            this.InvoiceSourcePath.Location = new System.Drawing.Point(121, 28);
            this.InvoiceSourcePath.Name = "InvoiceSourcePath";
            this.InvoiceSourcePath.Size = new System.Drawing.Size(311, 20);
            this.InvoiceSourcePath.TabIndex = 1;
            // 
            // InvoiceSourcePathButton
            // 
            this.InvoiceSourcePathButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.InvoiceSourcePathButton.Location = new System.Drawing.Point(438, 28);
            this.InvoiceSourcePathButton.Name = "InvoiceSourcePathButton";
            this.InvoiceSourcePathButton.Size = new System.Drawing.Size(75, 23);
            this.InvoiceSourcePathButton.TabIndex = 2;
            this.InvoiceSourcePathButton.Text = "Procurar";
            this.InvoiceSourcePathButton.UseVisualStyleBackColor = true;
            this.InvoiceSourcePathButton.Click += new System.EventHandler(this.InvoiceSourcePathButton_Click);
            // 
            // LabelTemplateFileButton
            // 
            this.LabelTemplateFileButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.LabelTemplateFileButton.Location = new System.Drawing.Point(438, 28);
            this.LabelTemplateFileButton.Name = "LabelTemplateFileButton";
            this.LabelTemplateFileButton.Size = new System.Drawing.Size(75, 23);
            this.LabelTemplateFileButton.TabIndex = 10;
            this.LabelTemplateFileButton.Text = "Procurar";
            this.LabelTemplateFileButton.UseVisualStyleBackColor = true;
            this.LabelTemplateFileButton.Click += new System.EventHandler(this.LabelTemplateFileButton_Click);
            // 
            // LabelTemplateFile
            // 
            this.LabelTemplateFile.Location = new System.Drawing.Point(121, 30);
            this.LabelTemplateFile.Name = "LabelTemplateFile";
            this.LabelTemplateFile.Size = new System.Drawing.Size(311, 20);
            this.LabelTemplateFile.TabIndex = 9;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(15, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Caminho do arquivo:";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.InvoiceDestinationPathButton);
            this.groupBox1.Controls.Add(this.InvoiceDestinationPath);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.InvoiceSourcePath);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.InvoiceSourcePathButton);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(523, 96);
            this.groupBox1.TabIndex = 18;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = " NF-e ";
            // 
            // InvoiceDestinationPathButton
            // 
            this.InvoiceDestinationPathButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.InvoiceDestinationPathButton.Location = new System.Drawing.Point(438, 52);
            this.InvoiceDestinationPathButton.Name = "InvoiceDestinationPathButton";
            this.InvoiceDestinationPathButton.Size = new System.Drawing.Size(75, 23);
            this.InvoiceDestinationPathButton.TabIndex = 4;
            this.InvoiceDestinationPathButton.Text = "Procurar";
            this.InvoiceDestinationPathButton.UseVisualStyleBackColor = true;
            this.InvoiceDestinationPathButton.Click += new System.EventHandler(this.InvoiceDestinationPathButton_Click);
            // 
            // InvoiceDestinationPath
            // 
            this.InvoiceDestinationPath.Location = new System.Drawing.Point(121, 54);
            this.InvoiceDestinationPath.Name = "InvoiceDestinationPath";
            this.InvoiceDestinationPath.Size = new System.Drawing.Size(311, 20);
            this.InvoiceDestinationPath.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 54);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(91, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Pasta de Destino:";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.PdfPrinterName);
            this.groupBox2.Controls.Add(this.LabelPrinterName);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Location = new System.Drawing.Point(541, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(398, 96);
            this.groupBox2.TabIndex = 19;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = " Impressora ";
            // 
            // PdfPrinterName
            // 
            this.PdfPrinterName.FormattingEnabled = true;
            this.PdfPrinterName.Location = new System.Drawing.Point(150, 53);
            this.PdfPrinterName.Name = "PdfPrinterName";
            this.PdfPrinterName.Size = new System.Drawing.Size(235, 21);
            this.PdfPrinterName.TabIndex = 10;
            // 
            // LabelPrinterName
            // 
            this.LabelPrinterName.FormattingEnabled = true;
            this.LabelPrinterName.Location = new System.Drawing.Point(150, 25);
            this.LabelPrinterName.Name = "LabelPrinterName";
            this.LabelPrinterName.Size = new System.Drawing.Size(235, 21);
            this.LabelPrinterName.TabIndex = 9;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(10, 56);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(136, 13);
            this.label8.TabIndex = 12;
            this.label8.Text = "Nome da Impressora Laser:";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 30);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(138, 13);
            this.label7.TabIndex = 9;
            this.label7.Text = "Nome da Impressora Zebra:";
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.AdobePath);
            this.groupBox3.Controls.Add(this.label4);
            this.groupBox3.Controls.Add(this.AdobePathButton);
            this.groupBox3.Location = new System.Drawing.Point(12, 194);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(523, 62);
            this.groupBox3.TabIndex = 20;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Adobe Acobat Reader";
            // 
            // AdobePath
            // 
            this.AdobePath.Location = new System.Drawing.Point(121, 26);
            this.AdobePath.Name = "AdobePath";
            this.AdobePath.Size = new System.Drawing.Size(311, 20);
            this.AdobePath.TabIndex = 7;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 29);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(113, 13);
            this.label4.TabIndex = 15;
            this.label4.Text = "Caminho do programa:";
            // 
            // AdobePathButton
            // 
            this.AdobePathButton.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.AdobePathButton.Location = new System.Drawing.Point(438, 24);
            this.AdobePathButton.Name = "AdobePathButton";
            this.AdobePathButton.Size = new System.Drawing.Size(75, 23);
            this.AdobePathButton.TabIndex = 8;
            this.AdobePathButton.Text = "Procurar";
            this.AdobePathButton.UseVisualStyleBackColor = true;
            this.AdobePathButton.Click += new System.EventHandler(this.AdobePathButton_Click);
            // 
            // Close
            // 
            this.Close.DialogResult = System.Windows.Forms.DialogResult.Cancel;
            this.Close.Location = new System.Drawing.Point(846, 217);
            this.Close.Name = "Close";
            this.Close.Size = new System.Drawing.Size(95, 33);
            this.Close.TabIndex = 13;
            this.Close.Text = "&Fechar";
            this.Close.UseVisualStyleBackColor = true;
            this.Close.Click += new System.EventHandler(this.Close_Click);
            // 
            // Save
            // 
            this.Save.Location = new System.Drawing.Point(745, 217);
            this.Save.Name = "Save";
            this.Save.Size = new System.Drawing.Size(95, 33);
            this.Save.TabIndex = 12;
            this.Save.Text = "&Salvar";
            this.Save.UseVisualStyleBackColor = true;
            this.Save.Click += new System.EventHandler(this.Save_Click);
            // 
            // groupBox4
            // 
            this.groupBox4.Controls.Add(this.LabelTemplateFile);
            this.groupBox4.Controls.Add(this.LabelTemplateFileButton);
            this.groupBox4.Controls.Add(this.label3);
            this.groupBox4.Location = new System.Drawing.Point(12, 114);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(523, 74);
            this.groupBox4.TabIndex = 21;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = " Etiqueta ";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.DbServerName);
            this.groupBox5.Controls.Add(this.label5);
            this.groupBox5.Location = new System.Drawing.Point(541, 114);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(398, 74);
            this.groupBox5.TabIndex = 22;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Banco de Dados";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(66, 38);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(95, 13);
            this.label5.TabIndex = 14;
            this.label5.Text = "Nome do Servidor:";
            // 
            // DbServerName
            // 
            this.DbServerName.Location = new System.Drawing.Point(167, 35);
            this.DbServerName.Name = "DbServerName";
            this.DbServerName.Size = new System.Drawing.Size(218, 20);
            this.DbServerName.TabIndex = 11;
            // 
            // Configuration
            // 
            this.AcceptButton = this.Save;
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CancelButton = this.Close;
            this.ClientSize = new System.Drawing.Size(947, 265);
            this.Controls.Add(this.groupBox5);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.Save);
            this.Controls.Add(this.Close);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Configuration";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Configurações";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox InvoiceSourcePath;
        private System.Windows.Forms.Button InvoiceSourcePathButton;
        private System.Windows.Forms.Button LabelTemplateFileButton;
        private System.Windows.Forms.TextBox LabelTemplateFile;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button InvoiceDestinationPathButton;
        private System.Windows.Forms.TextBox InvoiceDestinationPath;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox AdobePath;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button AdobePathButton;
        private System.Windows.Forms.Button Close;
        private System.Windows.Forms.Button Save;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.OpenFileDialog openFileDialog;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.ComboBox PdfPrinterName;
        private System.Windows.Forms.ComboBox LabelPrinterName;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox DbServerName;
    }
}