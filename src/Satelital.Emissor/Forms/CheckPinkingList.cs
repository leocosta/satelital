﻿using System;
using System.Linq;
using System.Windows.Forms;
using Satelital.Emissor.IoC;
using Satelital.Emissor.Model;
using Satelital.Emissor.Service;

namespace Satelital.Emissor.Forms
{
    public partial class CheckPinkingList : Form
    {
        #region Fields
        private readonly IPickingListService _pickingListService;
        private readonly PickingList _pickingList;
        #endregion

        #region Constructors
        public CheckPinkingList(int pickingListId)
        {
            _pickingListService = ServiceLocator.GetInstance<IPickingListService>();
            _pickingList = GetPickingList(pickingListId);

            InitializeComponent();
            InitializePickingListGrid();
            LoadPickingListInfo();
        }
        #endregion

        #region Initializers
        private void InitializePickingListGrid()
        {
            PickingListGrid.AutoGenerateColumns = true;
            LoadPickingListGrid();

            PickingListGrid.Columns["Id"].Visible = false;
            PickingListGrid.Columns["PickingList"].Visible = false;
            PickingListGrid.Columns["CheckDate"].Visible = false;

            PickingListGrid.Columns["Sku"].DisplayIndex = 0;
            PickingListGrid.Columns["Description"].DisplayIndex = 1;
            PickingListGrid.Columns["Description"].HeaderText = @"Descrição";
            PickingListGrid.Columns["Quantity"].DisplayIndex = 2;
            PickingListGrid.Columns["Quantity"].HeaderText = @"Qtd";
            PickingListGrid.Columns["SupplierOrderNumber"].DisplayIndex = 3;
            PickingListGrid.Columns["SupplierOrderNumber"].HeaderText = @"Doc. Fornecedor";
            PickingListGrid.Columns["Altsku"].DisplayIndex = 4;
            PickingListGrid.Columns["IsChecked"].DisplayIndex = 7;
            PickingListGrid.Columns["IsChecked"].HeaderText = @"Conferido";
            PickingListGrid.Columns["CheckedBy"].DisplayIndex = 8;
            PickingListGrid.Columns["CheckedBy"].HeaderText = @"Conferido por";
        }
        #endregion

        #region Form Methods
        private void LoadPickingListGrid()
        {
            PickingListGrid.DataSource = _pickingList.PickingListItems.ToList();
        }

        private void PickingListGrid_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (PickingListGrid.Rows.Count <= 0 || e.RowIndex < 0) return;

            var dialogResult = MessageBox.Show(@"Para conferir o item manualmente é necessária a autorização de um supervisor. Deseja continuar?", "",
                                         MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (dialogResult.Equals(DialogResult.Yes))
            {
                ManualCheckPinkingListItem(AuthorizeUser(), PickingListGrid.Rows[e.RowIndex].Cells["SupplierOrderNumber"].Value.ToString(), PickingListGrid.Rows[e.RowIndex].Cells["Altsku"].Value.ToString());
            }
        }


        #endregion

        #region PckginList Methods
        private PickingList GetPickingList(int pickingListId)
        {
            return _pickingListService.GetPickingList(pickingListId);
        }

        private void CheckPinkingListItem()
        {
            try
            {
                _pickingListService.CheckPickingListItem(UserSession.ActiveUser, _pickingList, SupplierOrderNumber.Text, Altsku.Text);
                AdviceForMoreThanOneItem(SupplierOrderNumber.Text);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void AdviceForMoreThanOneItem(string supplierOrderNumber)
        {
            if (_pickingList.HasMoreThanOneItemBy(supplierOrderNumber))
                MessageBox.Show(String.Format(@"ATENÇÃO: O pedido {0} possui mais de um item", supplierOrderNumber), @"Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void ManualCheckPinkingListItem(User authorizedBy, string supplierOrderNumber, string altsku)
        {
            try
            {
                _pickingListService.ManualCheckPinkingListItem(authorizedBy, _pickingList, supplierOrderNumber, altsku);
                LoadPickingListGrid();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void LoadPickingListInfo()
        {
            RatesConferredProgressBar.Value = _pickingList.RatesConferred;
            RatesConferredLabel.Text = String.Format("{0:n0}% Itens conferidos", _pickingList.RatesConferred);
            PickingListInfo.Text = String.Format("Importada em {0:dd/MM/yyyy} por {1}", _pickingList.CreateDate, _pickingList.CreatedBy.Name);
        }
        
        private static User AuthorizeUser()
        {
            using (var frm = new UserAuthentication())
            {
                frm.ShowDialog();
                return frm.UserAuthenticated;
            }
        }
        #endregion
        
        #region "Form Events"
        private void Close_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void ItemConferred_Click(object sender, EventArgs e)
        {
            CheckPinkingListItem();
            LoadPickingListGrid();
            LoadPickingListInfo();
            ClearForm();
        }

        private void ClearForm()
        {
            SupplierOrderNumber.Text = String.Empty;
            Altsku.Text = String.Empty;
            SupplierOrderNumber.Focus();
        }

        private void SupplierOrderNumber_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Tab || e.KeyCode == Keys.Enter)
            {
                Altsku.Focus();
            }
        }

        private void Altsku_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Tab || e.KeyCode == Keys.Enter)
            {
                ItemConferred_Click(this, null);
            }
        }
        #endregion

  
    }
}
