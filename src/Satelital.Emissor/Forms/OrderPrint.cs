﻿using System;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using Satelital.Emissor.IoC;
using Satelital.Emissor.Model;
using Satelital.Emissor.Service;

namespace Satelital.Emissor.Forms
{
    public partial class OrderPrint : Form
    {
        private readonly IInvoiceService _invoiceService;

        public OrderPrint()
        {
            _invoiceService = ServiceLocator.GetInstance<IInvoiceService>();

            InitializeComponent();
        }
        
        private void Print_Click(object sender, EventArgs e)
        {

            if (String.IsNullOrWhiteSpace(OrderId.Text))
            {

                MessageBox.Show(@"Você precisa informar o número do pedido", "", MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
                return;
            }

            var invoice = _invoiceService.Find(OrderId.Text);
            if (invoice == null)
            {
                MessageBox.Show(@"Pedido não encontrado!", "", MessageBoxButtons.OK,
                                MessageBoxIcon.Information);
                return;
            }

            if (invoice.IsProcessed)
            {
                if(MessageBox.Show(@"O pedido já foi processado. Deseja imprimi-lo novamente?", "", MessageBoxButtons.YesNo,
                                MessageBoxIcon.Question) != DialogResult.Yes)
                {
                    return;
                }
            }

            MoveProcessedFiles(invoice.ReferenceFile);

            PrintPDF(invoice.ReferenceFile);

            PrintLabel(invoice);

            ClearForm();
        }

        private void PrintPDF(string referenceFileName)
        {

            try
            {
                toolStripStatusLabel.Text = string.Format("Imprimindo NFe do pedido no. {0}", OrderId.Text);
                _invoiceService.PrintPDF(Path.Combine(ApplicationSettings.InvoiceDestinationPath, referenceFileName + ".pdf"));
            }
            catch (Exception e)
            {
                MessageBox.Show(@"Ocorreu um erro ao imprimir PDF: " + e.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void PrintLabel(Invoice invoice)
        {
            try
            {
                toolStripStatusLabel.Text = @"Imprimindo etiqueta de destino...";
                _invoiceService.PrintLabel(invoice, ApplicationSettings.LabelTemplateFile);
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format(@"Ocorreu um erro ao imprimir etiqueta: {0}", ex.Message), "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void MoveProcessedFiles(string referenceFileName)
        {
            if (ApplicationSettings.MoveProcessedFiles == false) return;

            try
            {
                _invoiceService.MoveProcessedFiles(referenceFileName, ApplicationSettings.InvoiceDestinationPath);
            }
            catch (Exception ex)
            {
                MessageBox.Show(String.Format(@"Ocorreu um erro ao mover arquivos processados: {0}", ex.Message));
            }
        }

        private void ClearForm()
        {
            OrderId.Text = string.Empty;
            OrderId.Focus();
            Thread.Sleep(4000);
            toolStripStatusLabel.Text = string.Format("Impressão concluída!");

        }

        private void ExitApplication_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void OrderId_TextChanged(object sender, EventArgs e)
        {

        }

    }
}
