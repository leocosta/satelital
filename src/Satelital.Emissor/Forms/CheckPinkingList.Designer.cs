﻿namespace Satelital.Emissor.Forms
{
    partial class CheckPinkingList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PickingListInfo = new System.Windows.Forms.Label();
            this.PickingListGrid = new System.Windows.Forms.DataGridView();
            this.ItemConferred = new System.Windows.Forms.Button();
            this.SupplierOrderNumber = new System.Windows.Forms.TextBox();
            this.ItemLabel = new System.Windows.Forms.Label();
            this.Close = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.Altsku = new System.Windows.Forms.TextBox();
            this.CheckItemsPanel = new System.Windows.Forms.Panel();
            this.RatesConferredLabel = new System.Windows.Forms.Label();
            this.RatesConferredProgressBar = new System.Windows.Forms.ProgressBar();
            ((System.ComponentModel.ISupportInitialize)(this.PickingListGrid)).BeginInit();
            this.CheckItemsPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // PickingListInfo
            // 
            this.PickingListInfo.AutoSize = true;
            this.PickingListInfo.Location = new System.Drawing.Point(12, 12);
            this.PickingListInfo.Name = "PickingListInfo";
            this.PickingListInfo.Size = new System.Drawing.Size(200, 13);
            this.PickingListInfo.TabIndex = 0;
            this.PickingListInfo.Text = "Lista importada em 21/12/11 por Fulano:";
            // 
            // PickingListGrid
            // 
            this.PickingListGrid.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.PickingListGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.PickingListGrid.Location = new System.Drawing.Point(12, 33);
            this.PickingListGrid.MultiSelect = false;
            this.PickingListGrid.Name = "PickingListGrid";
            this.PickingListGrid.ReadOnly = true;
            this.PickingListGrid.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.PickingListGrid.Size = new System.Drawing.Size(884, 278);
            this.PickingListGrid.TabIndex = 1;
            this.PickingListGrid.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.PickingListGrid_CellDoubleClick);
            // 
            // ItemConferred
            // 
            this.ItemConferred.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemConferred.Location = new System.Drawing.Point(777, 29);
            this.ItemConferred.Name = "ItemConferred";
            this.ItemConferred.Size = new System.Drawing.Size(104, 29);
            this.ItemConferred.TabIndex = 3;
            this.ItemConferred.Text = "&Conferido";
            this.ItemConferred.UseVisualStyleBackColor = true;
            this.ItemConferred.Click += new System.EventHandler(this.ItemConferred_Click);
            // 
            // SupplierOrderNumber
            // 
            this.SupplierOrderNumber.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SupplierOrderNumber.Location = new System.Drawing.Point(9, 29);
            this.SupplierOrderNumber.Name = "SupplierOrderNumber";
            this.SupplierOrderNumber.Size = new System.Drawing.Size(184, 27);
            this.SupplierOrderNumber.TabIndex = 1;
            this.SupplierOrderNumber.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SupplierOrderNumber_KeyDown);
            // 
            // ItemLabel
            // 
            this.ItemLabel.AutoSize = true;
            this.ItemLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ItemLabel.Location = new System.Drawing.Point(6, 10);
            this.ItemLabel.Name = "ItemLabel";
            this.ItemLabel.Size = new System.Drawing.Size(128, 16);
            this.ItemLabel.TabIndex = 4;
            this.ItemLabel.Text = "Doc. Fornecedor:";
            // 
            // Close
            // 
            this.Close.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Close.Location = new System.Drawing.Point(792, 430);
            this.Close.Name = "Close";
            this.Close.Size = new System.Drawing.Size(104, 29);
            this.Close.TabIndex = 5;
            this.Close.Text = "&Fechar";
            this.Close.UseVisualStyleBackColor = true;
            this.Close.Click += new System.EventHandler(this.Close_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(251, 10);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(142, 16);
            this.label1.TabIndex = 8;
            this.label1.Text = "Código do Produto:";
            // 
            // Altsku
            // 
            this.Altsku.Font = new System.Drawing.Font("Microsoft Sans Serif", 13F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Altsku.Location = new System.Drawing.Point(254, 29);
            this.Altsku.Name = "Altsku";
            this.Altsku.Size = new System.Drawing.Size(517, 27);
            this.Altsku.TabIndex = 2;
            this.Altsku.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Altsku_KeyDown);
            // 
            // CheckItemsPanel
            // 
            this.CheckItemsPanel.Controls.Add(this.SupplierOrderNumber);
            this.CheckItemsPanel.Controls.Add(this.label1);
            this.CheckItemsPanel.Controls.Add(this.Altsku);
            this.CheckItemsPanel.Controls.Add(this.ItemConferred);
            this.CheckItemsPanel.Controls.Add(this.ItemLabel);
            this.CheckItemsPanel.Location = new System.Drawing.Point(15, 333);
            this.CheckItemsPanel.Name = "CheckItemsPanel";
            this.CheckItemsPanel.Size = new System.Drawing.Size(884, 67);
            this.CheckItemsPanel.TabIndex = 9;
            // 
            // RatesConferredLabel
            // 
            this.RatesConferredLabel.AccessibleDescription = "";
            this.RatesConferredLabel.AutoSize = true;
            this.RatesConferredLabel.Location = new System.Drawing.Point(782, 314);
            this.RatesConferredLabel.Name = "RatesConferredLabel";
            this.RatesConferredLabel.Size = new System.Drawing.Size(111, 13);
            this.RatesConferredLabel.TabIndex = 13;
            this.RatesConferredLabel.Text = "100% Itens conferidos";
            // 
            // RatesConferredProgressBar
            // 
            this.RatesConferredProgressBar.Location = new System.Drawing.Point(12, 314);
            this.RatesConferredProgressBar.Name = "RatesConferredProgressBar";
            this.RatesConferredProgressBar.Size = new System.Drawing.Size(764, 13);
            this.RatesConferredProgressBar.TabIndex = 12;
            // 
            // CheckPinkingList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(908, 472);
            this.Controls.Add(this.RatesConferredLabel);
            this.Controls.Add(this.RatesConferredProgressBar);
            this.Controls.Add(this.CheckItemsPanel);
            this.Controls.Add(this.Close);
            this.Controls.Add(this.PickingListGrid);
            this.Controls.Add(this.PickingListInfo);
            this.Name = "CheckPinkingList";
            this.Text = "Conferir Lista";
            ((System.ComponentModel.ISupportInitialize)(this.PickingListGrid)).EndInit();
            this.CheckItemsPanel.ResumeLayout(false);
            this.CheckItemsPanel.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label PickingListInfo;
        private System.Windows.Forms.DataGridView PickingListGrid;
        private System.Windows.Forms.Button ItemConferred;
        private System.Windows.Forms.TextBox SupplierOrderNumber;
        private System.Windows.Forms.Label ItemLabel;
        private System.Windows.Forms.Button Close;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox Altsku;
        private System.Windows.Forms.Panel CheckItemsPanel;
        private System.Windows.Forms.Label RatesConferredLabel;
        private System.Windows.Forms.ProgressBar RatesConferredProgressBar;


    }
}