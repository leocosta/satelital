﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Satelital.Emissor.Forms;
using Satelital.Emissor.IoC;
using Satelital.Emissor.Model;
using Satelital.Emissor.Service;

namespace Satelital.Emissor.FormsUsersGrid
{
    public partial class UserList : Form
    {
        private readonly IUserService _userService;

        public UserList()
        {
            _userService = ServiceLocator.GetInstance<IUserService>();
            
            InitializeComponent();
            InitializeUsersGrid();
        }

        private void InitializeUsersGrid()
        {
            UsersGrid.AutoGenerateColumns = true;
            LoadUsersGrid();
            UsersGrid.Columns["Id"].Visible = false;
            UsersGrid.Columns["Password"].Visible = false;
            UsersGrid.Columns["UserRoleObserver"].Visible = false;
            UsersGrid.Columns["PickingListItems"].Visible = false;
            UsersGrid.Columns["PickingLists"].Visible = false;

            UsersGrid.Columns["Name"].DisplayIndex = 0;
            UsersGrid.Columns["Name"].HeaderText = @"Nome";
            UsersGrid.Columns["Username"].DisplayIndex = 1;
            UsersGrid.Columns["Username"].HeaderText = @"Usuário";
            UsersGrid.Columns["UserRole"].DisplayIndex = 2;
            UsersGrid.Columns["UserRole"].HeaderText = @"Grupo";
        }

        private void LoadUsersGrid()
        {
            UsersGrid.DataSource = _userService.GetUsers();
        }

        private void Close_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void UsersGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (UsersGrid.Rows.Count < 0 || e.RowIndex < 0) return;

            new UserUpdate((int)UsersGrid.Rows[e.RowIndex].Cells["Id"].Value).ShowDialog();
            LoadUsersGrid();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            new UserInsert().ShowDialog();
            LoadUsersGrid();
        }

    }
}
