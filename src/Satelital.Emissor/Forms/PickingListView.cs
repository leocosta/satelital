﻿using System;
using System.Windows.Forms;
using Satelital.Emissor.IoC;
using Satelital.Emissor.Service;

namespace Satelital.Emissor.Forms
{
    public partial class PickingListView : Form
    {
        private readonly IPickingListService _pickingListService;
        public PickingListView()
        {
            _pickingListService = ServiceLocator.GetInstance<IPickingListService>();

            InitializeComponent();
            InitializePickingListGrid();
        }
        
        private void InitializePickingListGrid()
        {
            PickingListGrid.AutoGenerateColumns = true;
            LoadPickingListGrid();
            PickingListGrid.Columns["Id"].Visible = false;
            PickingListGrid.Columns["PickingListItems"].Visible = false;

            PickingListGrid.Columns["Filename"].DisplayIndex = 0;
            PickingListGrid.Columns["Filename"].HeaderText = @"Arquivo";
            PickingListGrid.Columns["CreateDate"].DisplayIndex = 1;
            PickingListGrid.Columns["CreateDate"].HeaderText = @"Data";
            PickingListGrid.Columns["CreatedBy"].DisplayIndex = 2;
            PickingListGrid.Columns["CreatedBy"].HeaderText = @"Criado por";
            PickingListGrid.Columns["TotalItems"].DisplayIndex = 3;
            PickingListGrid.Columns["TotalItems"].HeaderText = @"Itens";

            PickingListGrid.Columns["RatesConferred"].DisplayIndex = 4;
            PickingListGrid.Columns["RatesConferred"].HeaderText = @"Conferido";
            PickingListGrid.Columns["RatesConferred"].DefaultCellStyle.Format = @"0\%";
        }

        private void LoadPickingListGrid()
        {
            PickingListGrid.DataSource = _pickingListService.GetPickingLists();
        }

        private void Close_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void PickingListGrid_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (PickingListGrid.Rows.Count < 0 || e.RowIndex < 0) return;
            
            new CheckPinkingList((int)PickingListGrid.Rows[e.RowIndex].Cells["Id"].Value).ShowDialog();
            LoadPickingListGrid();
        }

      
    }
}
