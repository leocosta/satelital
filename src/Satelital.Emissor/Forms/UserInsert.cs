﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Satelital.Emissor.IoC;
using Satelital.Emissor.Model;
using Satelital.Emissor.Service;

namespace Satelital.Emissor.Forms
{
    public partial class UserInsert : Form
    {
        private readonly IUserService _userService;

        public UserInsert()
        {
            _userService = ServiceLocator.GetInstance<IUserService>();
            
            InitializeComponent();
            BindUserRole();
        }

        private void BindUserRole()
        {
            UserRole.DataSource = Enum.GetNames(typeof(UserRole));
        }

        private void Close_Click(object sender, EventArgs e)
        {
            Close();
        }
        
        private void Save_Click(object sender, EventArgs e)
        {
            try
            {
                _userService.AddUser(FullName.Text, Username.Text, Password.Text, ConfirmPassword.Text,
                                     (UserRole)Enum.Parse(typeof(UserRole), UserRole.SelectedValue.ToString(), true));
                MessageBox.Show(@"Usuário cadastrado com sucesso!", @"Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
                ClearForm();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, @"Erro", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void ClearForm()
        {
            FullName.Text = String.Empty;
            Username.Text = String.Empty;
            Password.Text = String.Empty;
            ConfirmPassword.Text = String.Empty;
            UserRole.SelectedIndex = 0;
        }
    }
}
