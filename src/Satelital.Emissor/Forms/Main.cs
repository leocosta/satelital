﻿using System;
using System.Windows.Forms;
using Satelital.Emissor.FormsUsersGrid;
using Satelital.Emissor.IoC;
using Satelital.Emissor.Model;
using Satelital.Emissor.Service;

namespace Satelital.Emissor.Forms
{
    public partial class Main : Form
    {
        private readonly IPickingListService _pickingListService;

        public Main()
        {
            _pickingListService = ServiceLocator.GetInstance<IPickingListService>();

            InitializeComponent();
            HideOperatorMenu();
        }

        private void HideOperatorMenu()
        {
            if (UserSession.ActiveUser.IsAdministrator()) return;

            configuraçõesToolStripMenuItem.Visible = false;
            usuáriosToolStripMenuItem.Visible = false;
            toolStripMenuItemSeparator1.Visible = false;
        }

        private PickingList ImportFile()
        {
            if (ImportFileDialog.ShowDialog() != DialogResult.OK)
            {
                MessageBox.Show(@"A importação do arquivo foi cancelada");
                return null;
            }

            PickingList pickingList = null;
            try
            {
                pickingList = _pickingListService.ImportPickingList(UserSession.ActiveUser, ImportFileDialog.FileName); 
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message, @"Aviso", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            return pickingList;

        } 


        private void ImportFileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            var pickingList = ImportFile();
            if (pickingList != null)
                new CheckPinkingList(pickingList.Id).ShowDialog();
        }

        private void ViewPickingListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new PickingListView().ShowDialog();
        }

        private void PickingListToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void imprimirToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new OrderPrint().ShowDialog();
        }

        private void sairToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Application.Exit();
                
        }

        private void usuáriosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            new UserList().ShowDialog();
        }

        private void configuraçõesToolStripMenuItem_Click(object sender, EventArgs e)
        {

            var frm = new Configuration();
            frm.Show();
        }

    }
}
