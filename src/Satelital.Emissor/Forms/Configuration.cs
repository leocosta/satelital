﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Satelital.Emissor.Forms
{
    public partial class Configuration : Form
    {
        public Configuration()
        {
            InitializeComponent();
            LoadApplictionInfos();
        }

        private void Close_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void LoadApplictionInfos()
        {
            InvoiceSourcePath.Text = ApplicationSettings.InvoiceSourcePath;
            InvoiceDestinationPath.Text = ApplicationSettings.InvoiceDestinationPath;
            LabelPrinterName.Text = ApplicationSettings.LabelPrinterName;
            PdfPrinterName.Text = ApplicationSettings.PDFPrinterName;
            LabelTemplateFile.Text = ApplicationSettings.LabelTemplateFile;
            AdobePath.Text = ApplicationSettings.AdobePath;
            DbServerName.Text = ApplicationSettings.DbServerName;
            LabelPrinterName.DataSource = GetPrinterNames();
            LabelPrinterName.SelectedItem = ApplicationSettings.LabelPrinterName;
            PdfPrinterName.DataSource = GetPrinterNames();
            PdfPrinterName.SelectedItem = ApplicationSettings.PDFPrinterName;
        }

        private void Save_Click(object sender, EventArgs e)
        {
            ApplicationSettings.InvoiceSourcePath = InvoiceSourcePath.Text;
            ApplicationSettings.InvoiceDestinationPath = InvoiceDestinationPath.Text;
            ApplicationSettings.LabelPrinterName = LabelPrinterName.Text;
            ApplicationSettings.PDFPrinterName = PdfPrinterName.Text;
            ApplicationSettings.LabelTemplateFile = LabelTemplateFile.Text;
            ApplicationSettings.AdobePath = AdobePath.Text;
            ApplicationSettings.DbServerName = DbServerName.Text;
            ApplicationSettings.Save();
            ApplicationSettings.Refresh();

            MessageBox.Show(@"Configurações salvas com sucesso!", @"Aviso", MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
        }

        private void ShowFolderBrowserDialog(Control controlToChange)
        {
            if (folderBrowserDialog.ShowDialog() != DialogResult.OK)
                return;

            controlToChange.Text = folderBrowserDialog.SelectedPath;
        }



        private void ShowFileBrowserDialog(Control controlToChange, string fileFilter)
        {
            openFileDialog.Filter = fileFilter;
            if (openFileDialog.ShowDialog() != DialogResult.OK)
                return;

            
            controlToChange.Text = openFileDialog.FileName;
        }


        private static IList<string> GetPrinterNames()
        {
            return PrinterSettings.InstalledPrinters.Cast<object>().Cast<string>().ToList();
        }

        private void InvoiceSourcePathButton_Click(object sender, EventArgs e)
        {
            ShowFolderBrowserDialog(InvoiceSourcePath);
        }

        private void InvoiceDestinationPathButton_Click(object sender, EventArgs e)
        {
            ShowFolderBrowserDialog(InvoiceDestinationPath);
        }

        private void LabelTemplateFileButton_Click(object sender, EventArgs e)
        {
            ShowFileBrowserDialog(LabelTemplateFile, FileFilter.Prn);
        }

        private void AdobePathButton_Click(object sender, EventArgs e)
        {
            ShowFileBrowserDialog(AdobePath, FileFilter.Exe);
        }

    }

    internal class FileFilter
    {
        public static string Prn
        {
            get { return @"Template impressora (*.prn) | *.prn"; }
        }

        public static string Exe
        {
            get { return @"Arquivo executável (*.exe) | *.exe"; }
        }
    }

}
