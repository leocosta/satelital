using System;
using System.Collections.Generic;
using System.Reflection;

namespace Satelital.Emissor.Helper
{
    public static class ObjectMapper
    {
        public static IEnumerable<dynamic> GetProperties(this object obj)
        {
            foreach (var propInfo in obj.GetType().GetProperties())
            {
                if (!IsGenericType(propInfo.PropertyType))
                    foreach (var propInfoChild in propInfo.PropertyType.GetProperties())
                        yield return GetNewProperty(propInfoChild, propInfo.GetValue(obj, null));
                else
                    yield return GetNewProperty(propInfo, obj);
            }
        }

        private static bool IsGenericType(Type type)
        {
            return type == typeof(String) || type == typeof(int);
        }

        private static dynamic GetNewProperty(PropertyInfo property, object objValue)
        {
            return new { property.Name, Value = property.GetValue(objValue, null) };
        }
    }
}