using System.IO;

namespace Satelital.Emissor.Helper
{
    public static class FileManagerExtension
    {
        public static string GetContentFile(this string filename)
        {
            if (!filename.IsFile()) return null;

            using (var reader = new StreamReader(filename))
            {
                return reader.ReadToEnd();
            }
        }

        public static string CombineWithPath(this string filename, string path)
        {
            return filename.IsFile() ? Path.Combine(path, new FileInfo(filename).Name) : null;
        }

        private static bool IsFile(this string filename)
        {
            return File.Exists(filename);
        }
    }
}
