﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Satelital.Emissor.Helper
{
    public static class StringExtension
    {
        public static string Right(this string param, int size)
        {
            return param.Substring(param.Length - size, size);
        }

        public static String RemoveDiacritics(this String s)
        {
            var normalizedString = s.Normalize(NormalizationForm.FormD);
            var stringBuilder = new StringBuilder();

            foreach (var c in normalizedString.Where(c => CharUnicodeInfo.GetUnicodeCategory(c) != UnicodeCategory.NonSpacingMark))
                stringBuilder.Append(c);

            return stringBuilder.ToString().Replace("#", String.Empty).Replace(" ", String.Empty);
        }
    }
}
