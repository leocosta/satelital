using System;
using System.Configuration;

namespace Satelital.Emissor
{
    public class ApplicationSettings
    {
        private static readonly Configuration ConfigFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
        private static readonly AppSettingsSection AppSetSec = ConfigFile.AppSettings;

        public static string InvoiceSourcePath
        {
            get { return ConfigurationManager.AppSettings["invoiceSourcePath"]; }
            set
            {
                if (value == null) throw new InvalidOperationException(@"Argumento inv�lido para InvoiceSourcePath");
                AppSetSec.Settings["invoiceSourcePath"].Value = value;
            }
        }

        public static string InvoiceDestinationPath
        {
            get { return ConfigurationManager.AppSettings["invoiceDestinationPath"]; }
            set
            {
                if (value == null) throw new InvalidOperationException(@"Argumento inv�lido para InvoiceDestinationPath");
                AppSetSec.Settings["invoiceDestinationPath"].Value = value;

            }
        }

        public static string LabelTemplateFile
        {
            get { return ConfigurationManager.AppSettings["labelTemplateFile"]; }
            set
            {
                if (value == null) throw new InvalidOperationException(@"Argumento inv�lido para LabelTemplateFile");
                AppSetSec.Settings["labelTemplateFile"].Value = value;

            }
        }

        public static string LabelPrinterName
        {
            get { return ConfigurationManager.AppSettings["labelPrinterName"]; }
            set
            {
                if (value == null) throw new InvalidOperationException(@"Argumento inv�lido para LabelPrinterName");
                AppSetSec.Settings["labelPrinterName"].Value = value;
            }
        }

        public static string PDFPrinterName
        {
            get { return ConfigurationManager.AppSettings["pdfPrinterName"]; }
            set
            {
                if (value == null) throw new InvalidOperationException(@"Argumento inv�lido para PDFPrinterName");
                AppSetSec.Settings["pdfPrinterName"].Value = value;
            }
        }

        public static string AdobePath
        {
            get { return ConfigurationManager.AppSettings["adobePath"]; }
            set
            {
                if (value == null) throw new InvalidOperationException(@"Argumento inv�lido para AdobePath");
                AppSetSec.Settings["adobePath"].Value = value;
            }
        }

        public static string DbServerName
        {
            get { return ConfigurationManager.AppSettings["dbServerName"]; }
            set
            {
                if (value == null) throw new InvalidOperationException(@"Argumento inv�lido para DbServerName");
                AppSetSec.Settings["dbServerName"].Value = value;
            }
        }

        public static bool MoveProcessedFiles
        {
            get { return bool.Parse(ConfigurationManager.AppSettings["moveProcessedFiles"]); }
        }

        public static string XlsConnectionString
        {
            get { return ConfigurationManager.ConnectionStrings["XlsConnectionString"].ToString(); }
            set
            {
                if (value == null) throw new InvalidOperationException(@"Argumento inv�lido para XlsConnectionString");
                AppSetSec.Settings["XlsConnectionString"].Value = value;
            }
        }

        public static void Save()
        {
            ConfigFile.Save();
        }

        public static void Refresh()
        {
            ConfigurationManager.RefreshSection("connectionStrings");
            ConfigurationManager.RefreshSection("appSettings");
        }
    }
}