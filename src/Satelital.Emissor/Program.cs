﻿using System;
using System.Data.Entity;
using System.Windows.Forms;
using Satelital.Emissor.Forms;
using Satelital.Emissor.Infrastructure.Data.EFDataContext;
using Satelital.Emissor.IoC;
using Satelital.Emissor.Model;

namespace Satelital.Emissor
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Database.SetInitializer(new SatelitalModelContextInitializer());

            Bootstrapper.ConfigureUnityContainer();
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            using(var frm = new UserAuthentication())
            {
                Application.Run(frm);
                if (!frm.IsUserAuthenticated) return;

                UserSession.SetActiveUser(frm.UserAuthenticated);
                Application.Run(new Main());
            }
            
        }
    }
}
