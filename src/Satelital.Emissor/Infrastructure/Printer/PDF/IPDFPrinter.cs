namespace Satelital.Emissor.Infrastructure.Printer.PDF
{
    public interface IPDFPrinter
    {
        void Print(string filename);
    }
}