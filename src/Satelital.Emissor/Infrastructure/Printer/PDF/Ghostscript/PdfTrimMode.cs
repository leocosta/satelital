﻿
namespace Satelital.Emissor.Infrastructure.Printer.PDF.Ghostscript
{
  public enum PdfTrimMode
  {
    PaperSize,
    TrimBox,
    CropBox
  }
}
