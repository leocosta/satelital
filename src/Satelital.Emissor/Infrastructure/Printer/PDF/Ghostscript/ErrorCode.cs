﻿
namespace Satelital.Emissor.Infrastructure.Printer.PDF.Ghostscript
{
  /// <summary>
  ///  Values that represent GhostScript error codes. 
  /// </summary>
  public enum ErrorCode
  {

    /// <summary>Success</summary>
    Success = 0,

    /// <summary>Fatal Error</summary>
    FatalError = -100
  }
}
