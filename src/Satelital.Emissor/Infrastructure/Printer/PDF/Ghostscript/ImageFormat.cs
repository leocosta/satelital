﻿
namespace Satelital.Emissor.Infrastructure.Printer.PDF.Ghostscript
{
  public enum ImageFormat
  {
    Unknown,
    BitmapMono,
    Bitmap8,
    Bitmap24,
    Jpeg,
    PngMono,
    Png8,
    Png24,
    TiffMono,
    Tiff24,
  }
}

