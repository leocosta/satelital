﻿
namespace Satelital.Emissor.Infrastructure.Printer.PDF.Ghostscript
{
  public enum GridFitMode
  {
    None,
    SkipPatentedInstructions,
    Topological,
    Mixed
  }
}
