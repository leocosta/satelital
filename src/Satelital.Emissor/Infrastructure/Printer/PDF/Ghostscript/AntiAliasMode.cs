﻿
namespace Satelital.Emissor.Infrastructure.Printer.PDF.Ghostscript
{
  public enum AntiAliasMode
  {
    None = 0,
    Low = 1,
    Medium = 2,
    High = 4,
  }
}
