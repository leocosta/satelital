﻿using System;

namespace Satelital.Emissor.Infrastructure.Printer.PDF.Ghostscript
{
  public delegate int StdioCallBack(IntPtr handle, IntPtr strptr, int len);
}
