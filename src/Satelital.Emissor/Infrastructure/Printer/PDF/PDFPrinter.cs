﻿using System;
using System.Diagnostics;
using System.Drawing.Printing;
using System.Security.Permissions;
using PdfSharp.Pdf.Printing;
using Satelital.Emissor.Infrastructure.Printer.PDF.PdfConverter;

namespace Satelital.Emissor.Infrastructure.Printer.PDF
{
    public class PDFPrinter : IPDFPrinter
    {
        private readonly string _printerName;
        private readonly string _adobePath;

        public PDFPrinter(string printerName, string adobePath)
        {
            _printerName = printerName;
            _adobePath = adobePath;
        }

        public void Print(string filename)
        {
            PrintWithCommandline(filename);
        }

        [PermissionSet(SecurityAction.LinkDemand, Name = "FullTrust")]
        public void PrintWithGhostScriptDll(string ghostScriptPath, string pdfFileName){

            var printDoc = new PrintDocument { DocumentName = pdfFileName };
            printDoc.PrintPage += (o, e) => e.Graphics.DrawImage(new Pdf2Image(pdfFileName).GetImage(), 0, 0);
            printDoc.Print();
        }

        public void PrintWithPDFSharp(string pdfFileName)
        {
            PdfFilePrinter.AdobeReaderPath = _adobePath;
            var printer = new PdfFilePrinter(pdfFileName, _printerName);
            printer.Print();
        }

        public void PrintWithCommandline(string pdfFileName)
        {
            var psInfo = new ProcessStartInfo
                             {
                                 FileName = _adobePath,
                                 Arguments = String.Format("/s /o /h /p{0}", pdfFileName),
                                 WindowStyle = ProcessWindowStyle.Hidden,
                                 CreateNoWindow = true,
                                 UseShellExecute = true
                             };

            var process = Process.Start(psInfo);
        }
    }
}
