using IZebraCommand = Satelital.Emissor.Infrastructure.Printer.Zebra.Commands.IZebraCommand;

namespace Satelital.Emissor.Infrastructure.Printer.Zebra.Printing
{
    public class ZebraPrinter : IRawPrinter, IZebraPrinter
    {
        private readonly string _jobName;
        private readonly string _rawPrinterName;

        public ZebraPrinter(string rawPrinterName) : this(rawPrinterName, "Zebra Printing")
        {
            
        }

        private ZebraPrinter(string rawPrinterName, string jobName)
        {
            _rawPrinterName = rawPrinterName;
            _jobName = jobName;
        }

        public void Print(string rawData)
        {
            RawPrinter.SendToPrinter(_jobName, rawData, _rawPrinterName);
        }

        public void Print(IZebraCommand command)
        {
            Print(command.ToZebraInstruction());
        }
    }
}