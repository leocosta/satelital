using IZebraCommand = Satelital.Emissor.Infrastructure.Printer.Zebra.Commands.IZebraCommand;

namespace Satelital.Emissor.Infrastructure.Printer.Zebra.Printing
{
    public interface IRawPrinter
    {
        void Print(string rawData);
        void Print(IZebraCommand command);
    }
}