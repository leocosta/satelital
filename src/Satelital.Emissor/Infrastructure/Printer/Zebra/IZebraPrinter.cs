namespace Satelital.Emissor.Infrastructure.Printer.Zebra
{
    public interface IZebraPrinter
    {
        void Print(string rawData);
    }
}