namespace Satelital.Emissor.Infrastructure.Printer.Zebra.Commands
{
    public interface IZebraCommand
    {
        string ToZebraInstruction();
    }
}