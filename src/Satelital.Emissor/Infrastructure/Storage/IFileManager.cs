﻿using System;
using System.Collections.Generic;

namespace Satelital.Emissor.Infrastructure.Storage
{
    public interface IFileManager
    {
        IList<string> GetAll();
        IList<string> Find(Func<string, bool> func);
        void Move(string sourceFile, string destinationFile);
        string SourcePath { get; set; }
        string DestinationPath { get; }
    }
}
