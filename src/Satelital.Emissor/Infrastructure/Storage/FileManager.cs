using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace Satelital.Emissor.Infrastructure.Storage
{
    public class FileManager : IFileManager
    {
        public string SourcePath { get; set; }
        public string DestinationPath { get; set; }

        public FileManager(string sourcePath, string destinationPath)
        {
            DestinationPath = destinationPath;
            SourcePath = sourcePath;
        }

        public IList<string> GetAll()
        {
            return Directory.GetFiles(Directory.GetParent(SourcePath).ToString(), "*.*", SearchOption.AllDirectories)
                .Select(f=>f.ToLower()).ToList();
        }

        public IList<string> Find(Func<string, bool> func)
        {
            return GetAll().Where(func).ToList();
        }

        public void Move(string sourceFile, string destinationFile)
        {
            if (!File.Exists(destinationFile))
                File.Move(sourceFile, destinationFile);
        }

    }
}