using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using Satelital.Emissor.Helper;

namespace Satelital.Emissor.Infrastructure.Data.Extensions
{
    public static class DataTableExtensions
    {
        public static List<dynamic> ToDynamic(this DataTable dt)
        {
            var dynRows = new List<dynamic>();
            foreach (DataRow row in dt.Rows)
            {
                dynamic dynColumns = new ExpandoObject();
                foreach (DataColumn column in dt.Columns)
                {
                    var dict = (IDictionary<string, object>) dynColumns;
                    dict.Add(column.ColumnName.RemoveDiacritics(), row[column]);
                }
                dynRows.Add(dynColumns);
            }
            return dynRows;
        }
    }
}