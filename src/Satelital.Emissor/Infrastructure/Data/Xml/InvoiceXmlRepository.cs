﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Linq.Expressions;
using System.Xml;
using System.Xml.Linq;
using System.Xml.XPath;
using Satelital.Emissor.Helper;
using Satelital.Emissor.Infrastructure.Data.Extensions;
using Satelital.Emissor.Infrastructure.Storage;
using Satelital.Emissor.Model;
using Satelital.Emissor.Repository;
using Satelital.Emissor.Infrastructure.Data.Extensions;

namespace Satelital.Emissor.Infrastructure.Data.Xml {
    public class InvoiceXmlRepository : IInvoiceXmlRepository {

        private readonly IFileManager _fileManager;
        private readonly IList<Invoice> _invoices;

        #region Constructor
        public InvoiceXmlRepository(IFileManager fileManager)
        {
            _fileManager = fileManager;
            _invoices = new List<Invoice>();
        }
        #endregion 

        #region Private Methods
        private Invoice ConvertXmlToObject(string xml)
        {
            var xmlObj = XmlToObjectParser.ParseFromXml(xml);
            var NFe = ((ExpandoObject)xmlObj).PropertyExists("nfeProc") ? xmlObj.nfeProc.NFe : xmlObj.NFe;

            var referenceFile = NFe.infNFe.Id.Replace("NFe", String.Empty);
            var orderId = NFe.infNFe.infAdic.infCpl.Split(new[] { "Ped_Num:" }, StringSplitOptions.None)[1].Trim();
            var transport = NFe.infNFe.transp.transporta.xNome;
            string cep = NFe.infNFe.dest.enderDest.CEP;

            string invoiceNumber = NFe.infNFe.ide.nNF;
            invoiceNumber = invoiceNumber.Length < 6 ? (new string(Convert.ToChar("0"), 6) + invoiceNumber).Right(6) : invoiceNumber;
            
            var destination = new Address()
            {
                Name = NFe.infNFe.dest.xNome,
                StreetAddress = NFe.infNFe.dest.enderDest.xLgr,
                Number = NFe.infNFe.dest.enderDest.nro,
                District = NFe.infNFe.dest.enderDest.xBairro,
                City = NFe.infNFe.dest.enderDest.xMun,
                State = NFe.infNFe.dest.enderDest.UF,
                ZipCode = String.Concat(cep.Substring(0,5), "-" ,cep.Substring(5,3)) 
            };
            
            return new Invoice(invoiceNumber, orderId, transport, destination, referenceFile, xml.ToLower().IndexOf(_fileManager.DestinationPath.ToLower()) > -1);
        }

        #endregion

        #region Public Methods
        public IEnumerable<Invoice> GetAll()
        {
            _fileManager.Find(f => f.EndsWith(".xml")).ToList()
            .ForEach(x => _invoices.Add(ConvertXmlToObject(x)));

            return _invoices;
        }

        public IEnumerable<Invoice> Find(Expression<Func<Invoice, bool>> predicate)
        {
            return _invoices.AsQueryable().Where(predicate);
        }

        public Invoice First(Expression<Func<Invoice, bool>> predicate)
        {
            return _invoices.AsQueryable().Where(predicate).FirstOrDefault();
        }

        public Invoice FindInvoiceByOrderId(string orderId)
        {
            return _fileManager.Find(f => f.EndsWith(".xml")).ToList()
                .Where(f => FindInFile(f, orderId))
                .Select(ConvertXmlToObject)
                .FirstOrDefault();
        }
        #endregion
        
        private static bool FindInFile(string filename, string content)
        {
            using (var reader = XmlReader.Create(filename))
            {
                var root = XDocument.Load(reader);
                var nsmgr = new XmlNamespaceManager(reader.NameTable);
                nsmgr.AddNamespace("ns", "http://www.portalfiscal.inf.br/nfe");

                try
                {

                    if (root.XPathSelectElement("//ns:infCpl", nsmgr) == null)
                        return false;

                    return root.XPathSelectElement("//ns:infCpl", nsmgr).Value
                        .Split(new[] { "Ped_Num:" }, StringSplitOptions.None)[1]
                        .Trim()
                        .Equals(content);
                    
                }
                catch (Exception ex)
                {
                    throw new Exception(string.Format(@"Erro ao localizar {0} no arquivo {1}: {2}", content, filename, ex.Message));
                }
            }

        }

    }
}
