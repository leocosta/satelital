﻿using Satelital.Emissor.Model;
using Satelital.Emissor.Repository;

namespace Satelital.Emissor.Infrastructure.Data.EFDataContext.Repositories
{
    public class UserRepository : EFGenericRepository<User>, IUserRepository
    {
        public UserRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }

    }
}
