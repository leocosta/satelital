﻿using Satelital.Emissor.Model;
using Satelital.Emissor.Repository;

namespace Satelital.Emissor.Infrastructure.Data.EFDataContext.Repositories
{
    public class PickingListItemRepository : EFGenericRepository<PickingListItem>, IPickingListItemRepository
    {
        public PickingListItemRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }

    }
}
