﻿using Satelital.Emissor.Model;
using Satelital.Emissor.Repository;

namespace Satelital.Emissor.Infrastructure.Data.EFDataContext.Repositories
{
    public class PickingListRepository : EFGenericRepository<PickingList>, IPickingListRepository
    {
        public PickingListRepository(IDatabaseFactory databaseFactory)
            : base(databaseFactory)
        {
        }

    }
}
