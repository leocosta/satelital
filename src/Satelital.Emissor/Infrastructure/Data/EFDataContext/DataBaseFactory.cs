﻿using Satelital.Emissor.Infrastructure.Data.EFDataContext.Repositories;

namespace Satelital.Emissor.Infrastructure.Data.EFDataContext
{
    public class DatabaseFactory : Disposable, IDatabaseFactory
    {
        private SatelitalModelContext _dataContext;
        public SatelitalModelContext Get()
        {
            return _dataContext ?? (_dataContext = new SatelitalModelContext());
        }
        protected override void DisposeCore()
        {
            if (_dataContext != null)
                _dataContext.Dispose();
        }
    }


}