﻿using System;

namespace Satelital.Emissor.Infrastructure.Data.EFDataContext
{
    public interface IDatabaseFactory : IDisposable
    {
        SatelitalModelContext Get();
    }
}
