﻿using System.Configuration;
using System.Data.Entity;
using Satelital.Emissor.Infrastructure.Data.EFDataContext.Configuration;
using Satelital.Emissor.Model;

namespace Satelital.Emissor.Infrastructure.Data.EFDataContext
{
    public class SatelitalModelContext : DbContext
    {
        private static readonly string ConnectionString = ConfigurationManager.ConnectionStrings["SatelitalModelContext"].ConnectionString.Replace("[dbServerName]", ConfigurationManager.AppSettings["dbServerName"]);
        public SatelitalModelContext()
            : base(ConnectionString)
        {
        }

        public IDbSet<User> Users { get; set; }
        public IDbSet<PickingList> PickingLists { get; set; }
        public IDbSet<PickingListItem> PickingListItems { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new UserConfiguration());
            modelBuilder.Configurations.Add(new PickingListConfiguration());
            modelBuilder.Configurations.Add(new PickingListItemConfiguration());

            base.OnModelCreating(modelBuilder);
        }

        public virtual void Commit()
        {
            base.SaveChanges();
        }


    }

    public class SatelitalModelContextInitializer : DropCreateDatabaseIfModelChanges<SatelitalModelContext>
    {
        protected override void Seed(SatelitalModelContext context)
        {
            context.Users.Add(new User()
            {
                Name = "Satelital",
                Username = "satelital",
                Password = "sat2500",
                UserRole = UserRole.Supervisor
            });

            context.Users.Add(new User()
            {
                Name = "Operador",
                Username = "operador",
                Password = "opr0590",
                UserRole = UserRole.Operador
            });

            context.SaveChanges();
        }
    }
}

