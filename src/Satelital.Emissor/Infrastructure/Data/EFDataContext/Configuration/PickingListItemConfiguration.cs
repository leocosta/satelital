﻿using System.Data.Entity.ModelConfiguration;
using Satelital.Emissor.Model;

namespace Satelital.Emissor.Infrastructure.Data.EFDataContext.Configuration
{
    public class PickingListItemConfiguration : EntityTypeConfiguration<PickingListItem>
    {
        public PickingListItemConfiguration()
        {
            HasKey(o => o.Id);

            ToTable("PickingListItems");
        }
    }
}
