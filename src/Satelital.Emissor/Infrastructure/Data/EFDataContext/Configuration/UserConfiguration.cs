﻿using System.Data.Entity.ModelConfiguration;
using Satelital.Emissor.Model;

namespace Satelital.Emissor.Infrastructure.Data.EFDataContext.Configuration
{
    public class UserConfiguration : EntityTypeConfiguration<User>
    {
        public UserConfiguration()
        {
            HasKey(o => o.Id);

            HasMany(p => p.PickingLists)
                .WithRequired(o => o.CreatedBy);

            HasMany(p => p.PickingListItems)
                .WithOptional(o => o.CheckedBy);
            
            Ignore(p=>p.UserRole);

            Property(p => p.UserRoleObserver)
                .HasColumnName("UserRole");

            ToTable("Users");
        }
    }
}
