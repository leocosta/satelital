﻿using System.Data.Entity.ModelConfiguration;
using Satelital.Emissor.Model;

namespace Satelital.Emissor.Infrastructure.Data.EFDataContext.Configuration
{
    public class PickingListConfiguration : EntityTypeConfiguration<PickingList>
    {
        public PickingListConfiguration()
        {
            HasKey(o => o.Id);

            ToTable("PickingLists");
        }
    }
}
