﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Text;
using Satelital.Emissor.Infrastructure.Data.Extensions;

namespace Satelital.Emissor.Infrastructure.Data.Xls
{
    public class XlsRepository : IXlsRepository, IDisposable
    {
        private readonly string _connection;
        public XlsRepository(string connection)
        {
            _connection = connection;
        }

        public string PathFile { get; set; }

        public string Filename
        {
            get
            {
                return Path.GetFileName(PathFile);
            }
        }

        public List<dynamic> GetAllFrom(string sheet)
        {
            var query = String.Format("select * from [{0}$]", sheet);
            var dataAdapter = new OleDbDataAdapter(query, String.Format(_connection, PathFile));
            var dataSet = new DataSet();
            dataAdapter.Fill(dataSet);

            return dataSet.Tables[0].ToDynamic();
        }

        public void Dispose()
        {
            
        }
        
    }


}
