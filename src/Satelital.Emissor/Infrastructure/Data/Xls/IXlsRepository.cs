using System.Collections.Generic;

namespace Satelital.Emissor.Infrastructure.Data.Xls
{
    public interface IXlsRepository
    {
        string PathFile { get; set; }
        string Filename { get; }
        List<dynamic> GetAllFrom(string sheet);
    }
}