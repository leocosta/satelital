﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace Satelital.Emissor.Model
{
    public class PickingList
    {
        public PickingList()
        {
        }

        public PickingList(User user, string filename)
        {
            CreateDate = DateTime.Now;
            PickingListItems = PickingListItems ?? new Collection<PickingListItem>();
            CreatedBy = user;
            Filename = filename;
        }

        public int Id { get; set; }
        public DateTime CreateDate { get; set; }
        public virtual User CreatedBy { get; set; }

        public virtual ICollection<PickingListItem> PickingListItems { get; private set; }
        public string Filename { get; set; }

        public int TotalItems
        {
            get { return PickingListItems.Count; }
        }

        public int RatesConferred
        {
            get { return (int)((PickingListItems.Count(c => c.IsChecked) / (decimal)TotalItems) * 100); }
        }

        public bool IsAllConferred()
        {
            return RatesConferred.Equals(100);
        }

        public void AddPickingListItem(string supplierOrderNumber, string description, int quantity, string sku, string altSku)
        {
            PickingListItems.Add(new PickingListItem()
            {
                SupplierOrderNumber = supplierOrderNumber,
                Description = description,
                Quantity = quantity,
                Sku = sku,
                AltSku = altSku
            });

        }

        public bool HasMoreThanOneItemBy(string supplierOrderNumber)
        {
            return PickingListItems.Count(s => s.SupplierOrderNumber.Equals(supplierOrderNumber)) > 0;
        }

    }
}
