﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Satelital.Emissor.Model
{
    public class UserSession
    {
        public static User ActiveUser { get; protected set; }

        public static void SetActiveUser(User user)
        {
            ActiveUser = user;
        }
    }
}
