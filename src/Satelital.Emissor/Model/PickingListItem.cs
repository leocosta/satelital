﻿using System;

namespace Satelital.Emissor.Model
{
    public class PickingListItem
    {
        public virtual int Id { get; set; }
        public virtual PickingList PickingList { get; set; }
        public virtual string SupplierOrderNumber { get; set; }
        public virtual string Sku { get; set; }
        public virtual string AltSku { get; set; }
        public virtual string Description { get; set; }
        public virtual int Quantity { get; set; }
        public virtual User CheckedBy { get; set; }
        public virtual DateTime? CheckDate { get; set; }

        public bool IsChecked
        {
            get { return CheckedBy != null; }
        }

        public void SetChecked(User checkedBy)
        {
            CheckedBy = checkedBy;
            CheckDate = DateTime.Now;
        }
    }

}
