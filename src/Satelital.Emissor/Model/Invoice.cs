﻿namespace Satelital.Emissor.Model
{
    public class Invoice
    {
        public Invoice(string invoiceId, string orderId, string transport, Address destination, string fileReference, bool isProcessed)
        {
            InvoiceId = invoiceId;
            OrderId = orderId;
            Transport = transport;
            Destination = destination;
            ReferenceFile = fileReference;
            IsProcessed = isProcessed;
        }


        public string InvoiceId { get; set; }
        public string OrderId { get; set; }
        public string Transport { get; set; }
        public Address Destination { get; set; }
        public string ReferenceFile { get; set; }
        public bool IsProcessed { get; protected set; }

    }
}
