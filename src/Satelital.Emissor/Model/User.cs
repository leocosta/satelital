﻿
using System.Collections.Generic;

namespace Satelital.Emissor.Model
{
    public class User 
    {
        public User()
        {
        }

        public User(string name, string username, string password, UserRole userRole)
        {
            Name = name;
            Username = username;
            Password = password;
            UserRole = userRole;
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public byte UserRoleObserver { get; set; }
        public UserRole UserRole
        {
            get { return (UserRole) UserRoleObserver; }
            set { UserRoleObserver = (byte) value; }
        }

        public virtual ICollection<PickingListItem> PickingListItems { get; set; }
        public virtual ICollection<PickingList> PickingLists { get; set; }

        public bool IsAdministrator()
        {
            return UserRole.Equals(UserRole.Supervisor);
        }

        public override string ToString()
        {
            return Name;
        }
    }
}
