﻿
using System.Web.Mvc;

namespace Satelital.Emissor.IoC
{
    public static class ServiceLocator
    {
        public static T GetInstance<T>()
        {
            return DependencyResolver.Current.GetService<T>();
        }


    }
}