﻿using System.Configuration;
using System.Web.Mvc;
using Microsoft.Practices.Unity;
using Satelital.Emissor.Infrastructure.Data.EFDataContext;
using Satelital.Emissor.Infrastructure.Data.EFDataContext.Repositories;
using Satelital.Emissor.Infrastructure.Data.Xls;
using Satelital.Emissor.Infrastructure.Data.Xml;
using Satelital.Emissor.Infrastructure.Printer.PDF;
using Satelital.Emissor.Infrastructure.Printer.Zebra;
using Satelital.Emissor.Infrastructure.Printer.Zebra.Printing;
using Satelital.Emissor.Infrastructure.Storage;
using Satelital.Emissor.Repository;
using Satelital.Emissor.Service;

namespace Satelital.Emissor.IoC
{
    public static class Bootstrapper
    {
        public static void ConfigureUnityContainer()
        {
            var container = new UnityContainer();
            var databaseFactory = new DatabaseFactory();
            container
                .RegisterType<IUnitOfWork, UnitOfWork>()
                .RegisterType<IFileManager, FileManager>(new InjectionConstructor(ApplicationSettings.InvoiceSourcePath, ApplicationSettings.InvoiceDestinationPath))
                .RegisterType<IInvoiceXmlRepository, InvoiceXmlRepository>()
                .RegisterType<IXlsRepository, XlsRepository>(new InjectionConstructor(ApplicationSettings.XlsConnectionString))
                .RegisterType<IUserRepository, UserRepository>(new InjectionConstructor(databaseFactory))
                .RegisterType<IPickingListRepository, PickingListRepository>(new InjectionConstructor(databaseFactory))
                .RegisterType<IPickingListItemRepository, PickingListItemRepository>(new InjectionConstructor(databaseFactory))
                .RegisterType<IPDFPrinter, PDFPrinter>(new InjectionConstructor(ApplicationSettings.PDFPrinterName, ApplicationSettings.AdobePath))
                .RegisterType<IZebraPrinter, ZebraPrinter>(new InjectionConstructor(ApplicationSettings.LabelPrinterName))
                .RegisterType<IInvoiceService, InvoiceService>()
                .RegisterType<IUserService, UserService>()
                .RegisterType<IPickingListService, PickingListService>();

            DependencyResolver.SetResolver(new UnityDependencyResolver(container));
        }
    }
}