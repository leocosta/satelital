﻿using Satelital.Emissor.Model;

namespace Satelital.Emissor.Repository
{
    public interface IPickingListRepository : IRepository<PickingList>
    {
    }
}