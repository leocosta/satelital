﻿using Satelital.Emissor.Model;

namespace Satelital.Emissor.Repository
{
    public interface IUserRepository : IRepository<User>
    {
    }
}