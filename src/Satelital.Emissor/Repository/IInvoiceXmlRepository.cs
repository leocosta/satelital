﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using Satelital.Emissor.Model;

namespace Satelital.Emissor.Repository {
    public interface IInvoiceXmlRepository {
        IEnumerable<Invoice> GetAll();
        IEnumerable<Invoice> Find(Expression<Func<Invoice, bool>> predicate);
        Invoice First(Expression<Func<Invoice, bool>> predicate);
        Invoice FindInvoiceByOrderId(string orderId);
    }
}
