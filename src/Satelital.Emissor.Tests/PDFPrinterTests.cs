﻿using NUnit.Framework;
using Satelital.Emissor.Infrastructure.Printer.PDF;

namespace Satelital.Emissor.Tests
{
    [TestFixture]
    public class PDFPrinterTests
    {
        private IPDFPrinter _pdfPrinter;
        
        [SetUp]
        public void Initialize()
        {
            _pdfPrinter = new PDFPrinter(@"\\ESTOQUE\HPLaserJ", @"C:\Program Files (x86)\Adobe\Reader 9.0\Reader\AcroRd32.exe");
        }

        [Test]
        [TestCase(@"C:\satelital\recebidos\35111101336140000106550010006055941103978540.pdf")]
        public void PrintPdfTest(string filename)
        {
            _pdfPrinter.Print(filename);
        }
    }
}
