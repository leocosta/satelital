﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data.Objects.DataClasses;
using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using Satelital.Emissor.Infrastructure.Data.EFDataContext;
using Satelital.Emissor.Infrastructure.Data.EFDataContext.Repositories;
using Satelital.Emissor.Model;
using Satelital.Emissor.Repository;

namespace Satelital.Emissor.Tests
{
    [TestFixture]
    public class PickingListRepositoryTests
    {
        private IPickingListRepository _pickingListRepository;
        private IUserRepository _userRepository;

        [SetUp]
        public void Initialize()
        {
            var db = new DatabaseFactory();

            _pickingListRepository = new PickingListRepository(db);
            _userRepository = new UserRepository(db);
        }

        [Test]
        public void ShouldIsertPickingList()
        {
            
            //arrange
            var user = _userRepository.Find(u => u.Username.Equals("Leo")).FirstOrDefault();

            var pickingList = new PickingList(user, "arquivo");
            pickingList.AddPickingListItem("0000","aaaa",2,"aaa","aaa");
            //act
            _pickingListRepository.Add(pickingList);
            _pickingListRepository.SaveChanges();

            //assert
            pickingList.Id.Should().BeGreaterThan(0);
        }
        

  
    }
}
