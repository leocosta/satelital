﻿using System.Linq;
using FluentAssertions;
using NUnit.Framework;
using Satelital.Emissor.Infrastructure.Data.Xml;
using Satelital.Emissor.Infrastructure.Storage;
using Satelital.Emissor.Repository;

namespace Satelital.Emissor.Tests
{
    [TestFixture]
    public class InvoiceRepositoryTests
    {
        private IInvoiceXmlRepository _invoiceRepository;
        private IFileManager _fileManager;
        private const string SourcePath = @"c:\satelital\recebidos";
        private const string DestinationPath = @"c:\satelital\processados";

        [SetUp]
        public void Initialize()
        {
            _fileManager = new FileManager(SourcePath, DestinationPath);
            _invoiceRepository = new InvoiceXmlRepository(_fileManager);
        }

        [Test]
        public void ShouldReturnAllInvoices()
        {
            //act
            var invoices = _invoiceRepository.GetAll();

            //assert
            invoices.Count().Should().BeGreaterThan(0);
        }
        
        [Test]
        public void ShouldReturnInvoiceByOrderId()
        {
            //arrange
            const string orderId = "77400000278258";
            
            //act
            var invoice = _invoiceRepository.FindInvoiceByOrderId(orderId);

            //assert
            invoice.OrderId.Should().BeEquivalentTo(orderId);
        }

  
    }
}
