﻿using System.Configuration;
using FluentAssertions;
using NUnit.Framework;
using Satelital.Emissor.Infrastructure.Data.Xls;

namespace Satelital.Emissor.Tests
{
    [TestFixture]
    public class XlsRepositoryTests
    {
        private IXlsRepository _xlsRepository;

        [SetUp]
        public void Initialize()
        {
            _xlsRepository = new XlsRepository(ConfigurationManager.ConnectionStrings["XlsConnectionString"].ToString());
        }

        [Test]
        public void GetAllItemsFromPickingList()
        {
            _xlsRepository.PathFile = @"C:\satelital\xls\PickingList1.xls";
            _xlsRepository.GetAllFrom("Sheet1").Count.Should().BeGreaterOrEqualTo(1);
        }
    }
}
