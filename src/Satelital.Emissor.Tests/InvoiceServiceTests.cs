﻿using FluentAssertions;
using NUnit.Framework;
using Satelital.Emissor.Infrastructure.Data.Xml;
using Satelital.Emissor.Infrastructure.Printer.PDF;
using Satelital.Emissor.Infrastructure.Printer.Zebra;
using Satelital.Emissor.Infrastructure.Printer.Zebra.Printing;
using Satelital.Emissor.Infrastructure.Storage;
using Satelital.Emissor.Repository;
using Satelital.Emissor.Service;

namespace Satelital.Emissor.Tests
{
    [TestFixture]
    public class InvoiceServiceTests
    {
        private const string SourcePath = @"c:\satelital\";
        private const string DestinationPath = @"c:\satelital\processados\";
        private const string LabelPrinterName = @"\\ESTOQUE\ZebraS600";
        private const string TemplatePath = @"c:\satelital\templates\LabelS600.prn";

        private IFileManager _fileManager;
        private IInvoiceXmlRepository _invoiceRepository;
        private IInvoiceService _invoiceService;

        private IZebraPrinter _zebraPrinter;
        private IPDFPrinter _pdfPrinter;

        [SetUp]
        public void Initialize()
        {
            //arrange

            _fileManager = new FileManager(SourcePath, DestinationPath);
            _invoiceRepository = new InvoiceXmlRepository(_fileManager);
            _zebraPrinter = new ZebraPrinter(LabelPrinterName);
            _pdfPrinter = new PDFPrinter(@"\\ESTOQUE\HPLaserJ", @"C:\Program Files (x86)\Adobe\Reader 9.0\Reader\AcroRd32.exe");

            _invoiceService = new InvoiceService(_fileManager, _invoiceRepository, _zebraPrinter, _pdfPrinter);
        }

        [Test]
        [TestCase("77340000235249")]
        [TestCase("77400000271057")]
        public void ShouldLocateInvoiceByOrderId(string orderId)
        {
            //act
            var invoice = _invoiceService.Find(orderId);

            //assert
            invoice.OrderId.Should().Match(orderId);
        }

        [Test]
        public void ShouldVerifyProcessedInvoice()
        {
            //assert
            const string orderId = "77340000235249";

            //act
            var invoice = _invoiceService.Find(orderId);

            //assert
            invoice.IsProcessed.Should().Be(true);
        }

        [Test]
        public void ShouldVerifyNonProcessedInvoice()
        {
            //assert
            const string orderId = "77340000235661";

            //act
            var invoice = _invoiceService.Find(orderId);

            //assert
            invoice.IsProcessed.Should().Be(false);
        }
        
        [Test]
        public void ShouldPrintInvoiceLabel()
        {
            //arrange
            const string orderId = "77340000234943";

            //act
            var invoice = _invoiceService.Find(orderId);
            _invoiceService.PrintLabel(invoice, TemplatePath);

        }

        [Test]
        public void MovingAllFiles()
        {

            //arrange
            var invoices = _invoiceRepository.GetAll();

            //act
            foreach (var invoice in invoices)
                _invoiceService.MoveProcessedFiles(invoice.ReferenceFile, DestinationPath);
        }
    }
}
