﻿using FluentAssertions;
using NUnit.Framework;
using Satelital.Emissor.Infrastructure.Data.EFDataContext;
using Satelital.Emissor.Infrastructure.Data.EFDataContext.Repositories;
using Satelital.Emissor.Model;

namespace Satelital.Emissor.Tests
{
    [TestFixture]
    public class UserRepositoryTests
    {
        private UserRepository _userRepository;

        [SetUp]
        public void Initialize()
        {
            _userRepository = new UserRepository(new DatabaseFactory());
        }

        [Test]
        public void ShouldIsertUser()
        {
            
            //arrange
            var user = new User() { Name = "Leonardo Costa", Password = "123123", Username = "LeoCosta" };
            
            //act
            _userRepository.Add(user);
            _userRepository.SaveChanges();

            //assert
            user.Id.Should().BeGreaterThan(0);
        }
        

  
    }
}
