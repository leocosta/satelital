﻿using System.Linq;
using NUnit.Framework;
using FluentAssertions;
using Satelital.Emissor.Infrastructure.Storage;

namespace Satelital.Emissor.Tests
{
    [TestFixture]
    public class FileManagerTests
    {
        private const string SourcePath = @"c:\satelital\recebidos";
        private const string DestinationPath = @"c:\satelital\processados";

        [Test]
        public void ShouldReturnAllFiles()
        {
            //arrange
            var fileManager = new FileManager(SourcePath, DestinationPath);
            
            //act
            var foundFiles = fileManager.GetAll();

            //assert
            foundFiles.Count().Should().BeGreaterThan(0);
        }

        [Test]
        [TestCase(".xml")]
        [TestCase(".pdf")]
        public void ShouldReturnFilesByExtension(string extension)
        {
            //arrange
            var fileManager = new FileManager(SourcePath, DestinationPath);
            
            //act
            var foundFiles = fileManager.Find(w => w.EndsWith(extension));

            //assert
            foundFiles.Count().Should().BeGreaterThan(0);
        }
    }
}
